import React from 'react';
import qr from '@vkontakte/vk-qr';
import TextToSVG from 'text-to-svg';
import wrap from 'word-wrap';
import {DEMI_BOLD, DEMI_REGULAR} from './data/fonts'
import {
  defaultOptions,
  defaultBackgroundColor,
  defaultForegroundColor,
  defaultActionForegroundColor,
  defaultActionBackgroundColor,
  sizes,
  ACTION_HEIGHT,
  ACTION_LINE_HEIGHT,
  ACTION_MAX_MAX_LINE_CHARS,

  SIZE_SMALL,
  SIZE_MEDIUM,
  SIZE_LARGE,
  SIZE_LARGE_GRADIENT

} from './config';
import './main.css';


class Widget extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      title: {},
      login: {},
      action: {},
      textToSvg: false,
      textToSvgLogin: false,
    };
  }

  getBackgroundImage() {
    return this.props.backgroundImage;
  }

  getOptions() {
    return this.props.options || defaultOptions;
  }

  getBackgroundColor() {
    return this.props.backgroundColor || defaultBackgroundColor;
  }

  getActionBackgroundColor(isForBackground) {
    if (isForBackground) {
      return 'rgba(0,0,0,0.5)';
    }

    return this.props.hasOwnProperty('actionBackgroundColor') && this.props.actionBackgroundColor
      ? this.props.actionBackgroundColor
      : defaultActionBackgroundColor;
  }

  getActionForegroundColor() {
    return this.props.hasOwnProperty('actionForegroundColor') && this.props.actionForegroundColor
      ? this.props.actionForegroundColor
      : defaultActionForegroundColor;
  }

  getLoginForegroundColor() {
    return this.props.hasOwnProperty('loginForegroundColor') && this.props.loginForegroundColor
      ? this.props.loginForegroundColor
      : defaultForegroundColor;
  }

  getTitleForegroundColor() {
    return this.props.hasOwnProperty('titleForegroundColor')
      ? this.props.titleForegroundColor
      : defaultForegroundColor;
  }

  getSuffix() {
    const {suffix} = this.props;

    if (suffix) {
      return suffix;
    }

    if (this.state.suffix) {
      return this.state.suffix;
    }

    this.setState({suffix: Math.round(Math.random() * 1e7)});
  }

  getSize() {
    const {labels} = this.props;
    const {withImageBackground, avatar, size} = this.props;

    const widgetSize = Object.assign({}, sizes[size]);

    if (size > 2) {
      if (!avatar) {
        widgetSize.height -= 56;
      }

      if (!labels.title) {
        widgetSize.height -= 23;
      }

      if (!labels.login) {
        widgetSize.height -= 15;
      }
    }

    if (size === SIZE_LARGE_GRADIENT && withImageBackground) {
      widgetSize.height += 53;
    }

    if (!labels.action) {
      if (size !== 2) {
        widgetSize.height -= 55;
      }

    } else {
      const lines = wrap(labels.action, {width: ACTION_MAX_MAX_LINE_CHARS, indent: '', cut: false}).split('\n').length;
      if (lines > 1) {
        widgetSize.height += (lines - 1) * ACTION_LINE_HEIGHT;
      }
    }

    return widgetSize;
  }

  getSvg(key, textToSVG) {
    const {labels} = this.props;

    const options = this.getOptions();
    switch (key) {
      case 'action':
        options[key].attributes.fill = this.getActionForegroundColor();
        break;
      case 'login':
        options[key].attributes.fill = this.getLoginForegroundColor();
        break;
      case 'title':
        options[key].attributes.fill = this.getTitleForegroundColor();
        break;
    }

    if (labels[key] && key === 'action') {
      labels[key] = wrap(labels[key], {width: ACTION_MAX_MAX_LINE_CHARS, indent: '', cut: false}).toUpperCase();

      if (labels[key].indexOf('\n') !== -1) {
        let labelsArray = labels[key].split('\n');
        let pathes = [];
        let metrics = [];
        for (let i = 0; i < labelsArray.length; i++) {
          pathes.push(textToSVG.getPath(labelsArray[i], options[key]));
          metrics.push(textToSVG.getMetrics(labelsArray[i], options[key]));
        }

        return {
          path: pathes,
          metrics: metrics,
          value: labels[key] || false
        };

      } else {
        return {
          path: [textToSVG.getPath(labels[key], options[key])],
          metrics: [textToSVG.getMetrics(labels[key], options[key])],
          value: labels[key] || false
        };
      }
    }

    return {
      path: [textToSVG.getPath(labels[key], options[key])],
      metrics: [textToSVG.getMetrics(labels[key], options[key])],
      value: labels[key] || false
    };
  }

  componentWillReceiveProps(newProps) {
    if (newProps.backgroundImage !== this.props.backgroundImage) {
      this.setState({averageBackgroundColor: false});
    }
    this.generateSvgText();
  }

  componentDidMount() {
    this.generateSvgText();
  }

  generateSvgText() {
    const self = this;
    TextToSVG.load(DEMI_BOLD, function(err, textToSVG) {
      const newState = {
        textToSVG: textToSVG,
        title: self.getSvg('title', textToSVG),
        action: self.getSvg('action', textToSVG)
      };
      self.setState(newState);
    });

    TextToSVG.load(DEMI_REGULAR, function(err, textToSVG) {
      self.setState({
        textToSVGLogin: textToSVG,
        login: self.getSvg('login', textToSVG)
      });
    });
  }

  static getAverageColor(image) {
    let canvas = document.createElement('canvas');
    let context = canvas.getContext && canvas.getContext('2d');
    let rgb = {r: 102, g: 102, b: 102};
    let pixelInterval = 5;
    let count = 0;
    let i = -4;
    let data, length;

    let height = canvas.height = image.naturalHeight || image.offsetHeight || image.height,
      width = canvas.width = image.naturalWidth || image.offsetWidth || image.width;

    context.drawImage(image, 0, 0);

    try {
      data = context.getImageData(0, 0, width, height);
    } catch (e) {
      console.error(e);
      return rgb;
    }

    data = data.data;
    length = data.length;
    while ((i += pixelInterval * 4) < length) {
      count++;
      rgb.r += data[i];
      rgb.g += data[i + 1];
      rgb.b += data[i + 2];
    }

    rgb.r = Math.floor(rgb.r / count).toString(16);
    rgb.g = Math.floor(rgb.g / count).toString(16);
    rgb.b = Math.floor(rgb.b / count).toString(16);

    return '#' + rgb.r + rgb.g + rgb.b;
  }

  generateCard(qrSvg) {
    const {isBigQr, withImageBackground, avatar, size} = this.props;
    const {averageBackgroundColor, login, title, action} = this.state;

    let actionBackgroundColor = this.getActionBackgroundColor();
    if (!login.path || !title.path || !action.path) {
      return '';
    }
    let body = '', defs = '';

    let {width, height} = this.getSize();

    let qrScale = 1;
    let qrPosition = {x: (width - 190) / 2, y: height - 190};
    if (isBigQr) {
      qrPosition = {x: 0, y: 220};
    }

    let actionPosition = {};
    if (action.value) {
      qrPosition.y -= 85;
    } else {
      qrPosition.y -= 30;
    }

    let actionDef = ``;
    let titleDef = ``;
    let loginDef = ``;
    let bgDef = ``;

    const avatarPosition = {x: (width - 56) / 2, y: 32};
    const titlePosition = {x: (width - title.metrics[0].width) / 2, y: avatarPosition.y + 56 + 33};
    const loginPosition = {x: (width - login.metrics[0].width) / 2, y: titlePosition.y + 25};
    const actionBackgroundPosition = {
      x: 0,
      y: height - ACTION_HEIGHT - ((action.path.length - 1) * ACTION_LINE_HEIGHT)
    };

    if (withImageBackground && this.getBackgroundImage()) {
      let image = new Image();
      image.src = this.getBackgroundImage();
      if (size === SIZE_LARGE_GRADIENT) {
        image.onload = () => {
          if (!averageBackgroundColor) {
            this.setState({averageBackgroundColor: Widget.getAverageColor(image)});
          }
        };

        if (!averageBackgroundColor) {
          return '';
        }
      }

      let backgroundImageHeight = size === SIZE_LARGE_GRADIENT ? 275 : height;
      body += `<image preserveAspectRatio="xMidYMid slice" x="0" y="0" width="${width}" height="${backgroundImageHeight}" xlink:href="${this.getBackgroundImage()}" />`;
      if (size === SIZE_LARGE_GRADIENT) {
        body += `
            <rect fill="url(#gradient-${this.getSuffix()})" x="0" y="184" width="${width}" height="90"/>
            <rect fill="${averageBackgroundColor}" x="0" y="274" width="${width}" height="${height}"/>`;
        bgDef = `
        <linearGradient id="gradient-${this.getSuffix()}" x1="0%" y1="0%" x2="0%" y2="100%">
          <stop offset="0%" style="stop-color:${averageBackgroundColor};stop-opacity:0" />
          <stop offset="100%" style="stop-color:${averageBackgroundColor};stop-opacity:1" />
        </linearGradient>`;
      }

      if (size === SIZE_LARGE_GRADIENT) {
        if (action.value) {
          actionBackgroundColor = this.getActionBackgroundColor(true);
        } else {
          qrPosition.y -= ACTION_HEIGHT;
        }

        titlePosition.y = height - ACTION_HEIGHT;
        loginPosition.y = titlePosition.y + 20;
      }
    } else {
      if (averageBackgroundColor) {
        this.setState({averageBackgroundColor: false});
      }
    }

    if (avatar && size !== SIZE_LARGE_GRADIENT) {
      body += `
        <circle cx="125" cy="60" r="30" fill="#fff"/>
        <image preserveAspectRatio="xMidYMid slice" clip-path="url(#avatar-mask-${this.getSuffix()})" x="${avatarPosition.x}" y="${avatarPosition.y}" width="56" height="56" xlink:href="${avatar}" />
        `;
    }

    if (title.value) {
      titleDef = `<g id="title-${this.getSuffix()}">${title.path[0]}</g>`;
      body += `<use x="${titlePosition.x}" y="${titlePosition.y}" xlink:href="#title-${this.getSuffix()}"/>`;
    }

    if (login.value) {
      loginDef = `<g id="login-${this.getSuffix()}">${login.path[0]}</g>`;
      body += `<use x="${loginPosition.x}" y="${loginPosition.y}" xlink:href="#login-${this.getSuffix()}"/>`;
    }

    if (action.value) {
      const actionBackgroundHeight = ACTION_HEIGHT + ((action.path.length - 1) * ACTION_LINE_HEIGHT);
      body += `<rect width="${width}" height="${actionBackgroundHeight}" transform="translate(${actionBackgroundPosition.x},${actionBackgroundPosition.y})" style="fill:${actionBackgroundColor};"/>`;
      actionPosition.y = actionBackgroundPosition.y + 30;
      qrPosition.y -= (action.path.length - 1) * ACTION_LINE_HEIGHT;
      for (let i = 0; i < action.path.length; i++) {
        body += `<use x="${(width - action.metrics[i].width) / 2}" y="${actionPosition.y}" xlink:href="#action-${this.getSuffix()}-${i}"/>`;
        actionPosition.y += ACTION_LINE_HEIGHT;
        actionDef += `<g id="action-${this.getSuffix()}-${i}">${action.path[i]}</g>`;
      }
    }

    body += `<use x="${qrPosition.x}" y="${qrPosition.y}" xlink:href="#qr-${this.getSuffix()}" transform="scale(${qrScale})" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"/>`

    return `
      <svg width="${width}" height="${height}" viewBox="0 0 ${width} ${height}" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <defs>
          <clipPath id="main-mask-${this.getSuffix()}">
            <rect x="0" y="0" width="${width}" height="${height}" rx="10"/>
          </clipPath>
          <clipPath id="avatar-mask-${this.getSuffix()}">
            <circle cx="125" cy="60" r="28" fill="#000"/>
          </clipPath>
          ${defs}
          ${bgDef}
          <g id="qr-${this.getSuffix()}">${qrSvg}</g>
          ${titleDef}
          ${loginDef}
          ${actionDef}
        </defs>
        <g clip-path="url(#main-mask-${this.getSuffix()})">
            <rect width="${width}" height="${height}" style="fill:${this.getBackgroundColor()}"/>
            ${body}
        </g>
      </svg>`;
  }

  render() {
    const {qrOptions, url} = this.props;

    qrOptions.suffix = this.getSuffix();

    const qrSvg = qr.createQR(url, 190, 'qr-code', qrOptions);

    return (<div>
      <span dangerouslySetInnerHTML={{__html: this.generateCard(qrSvg)}}/>
    </div>);
  }
}

export default Widget;
