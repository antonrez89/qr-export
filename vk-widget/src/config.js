export const attributes = { fill: 'white' };

export const defaultUrl = 'https://vk.com/';

export const defaultAvatar = 'https://vk.com/images/camera_200.png';

export const defaultActionBackgroundColor = '#3A6CA5';
export const defaultActionForegroundColor = '#FFF';

export const defaultBackgroundColor = '#4680C2';
export const defaultForegroundColor = '#FFF';

export const defaultLabels = {
    title: 'Заголовок',
    login: '@login',
    action: 'Написать сообщение'.toUpperCase()
};

export const defaultOptions = {
    title: {x: 0, y: 0, fontSize: 23, anchor: 'middle', attributes: attributes},
    login: {
        x: 0,
        y: 0,
        fontSize: 15,
        anchor: 'middle',
        attributes: {fill: 'rgba(255, 255, 255, 0.7)'}
    },
    action: {x: 0, y: 0, fontSize: 18, anchor: 'middle', attributes: attributes}
};

export const sizes = {
    0: { width: 250, height: 250 },
    1: { width: 250, height: 305 },
    2: { width: 250, height: 370 },
    3: { width: 250, height: 442 },
    4: { width: 250, height: 442 },
};

export const ACTION_HEIGHT = 54;
export const ACTION_LINE_HEIGHT = 20;
export const ACTION_MAX_MAX_LINE_CHARS = 21;

export const SIZE_SMALL = 1;
export const SIZE_MEDIUM = 2;
export const SIZE_LARGE = 3;
export const SIZE_LARGE_GRADIENT = 4;
