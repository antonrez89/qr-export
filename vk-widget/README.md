<div align="center">
<a href="https://github.com/VKCOM">

<img width="100" height="100" src="https://avatars3.githubusercontent.com/u/1478241?s=200&v=4">
</a>
<br>
<br>

</div>


# VK QR Widget
React-компонент для создания виджета с VK QR-кодом


## Подключение
```js
import QRWidget from '@vkontakte/vk-qr-widget-component';
```

## Примеры использования
```js
<QRWidget showForm={true} url={defaultUrl} avatar={defaultAvatar} background={loveTree}
```

```
<QRWidget url="https://vk.com" qrOptions={{
    isShowLogo: true, // show logo in center
    logoData: false, // logo data in base64
    isShowBackground: false, // show qr-background
    backgroundColor: '#fff', // qr-code background color
    foregroundColor: '#000000', // qr-code color
    }} backgroundColor="#fff" size={1} labels={{
        action: false,
        title: false,
        login: false,
}}/>
```
<img width="100" height="100" src="https://gitlab.mvk.com/vkapi/vk-qr-widget-component/raw/master/images/1.svg">

---
```
<QRWidget url="https://vk.com" qrOptions={{
    isShowLogo: true, // show logo in center
    logoData: false, // logo data in base64
    isShowBackground: false, // show qr-background
    backgroundColor: '#fff', // qr-code background color
    foregroundColor: '#000000', // qr-code color
    }} backgroundColor="#fff" size={1} labels={{
        action: "Написать сообщение",
        title: false,
        login: false,
}}/>
```
<img width="100" height="100" src="https://gitlab.mvk.com/vkapi/vk-qr-widget-component/raw/master/images/2.svg">

---
```
<QRWidget url="https://vk.com" qrOptions={{
    isShowLogo: true, // show logo in center
    logoData: false, // logo data in base64
    isShowBackground: false, // show qr-background
    backgroundColor: '#fff', // qr-code background color
    foregroundColor: '#000000', // qr-code color
    logoColor: '#000000',
    }} backgroundColor="#fff" actionBackgroundColor="#000" size={1} labels={{
        action: "Подписаться",
        title: false,
        login: false,
}}/>
```
<img width="100" height="100" src="https://gitlab.mvk.com/vkapi/vk-qr-widget-component/raw/master/images/3.svg">

---
```
<QRWidget url="https://vk.com" qrOptions={{
    isShowLogo: true, // show logo in center
    logoData: false, // logo data in base64
    isShowBackground: false, // show qr-background
    backgroundColor: '#fff', // qr-code background color
    foregroundColor: '#000000', // qr-code color
    }} backgroundColor="#fff" size={1} labels={{
        action: "Узнайте с помощью камеры VK и съешьте булку",
        title: false,
        login: false,
}}/>
```
<img width="100" height="100" src="https://gitlab.mvk.com/vkapi/vk-qr-widget-component/raw/master/images/4.svg">

---
```
<QRWidget url="https://vk.com" qrOptions={{
    isShowLogo: true, // show logo in center
    logoColor: '#000000',
    logoData: false, // logo data in base64
    isShowBackground: true, // show qr-background
    backgroundColor: '#fff', // qr-code background color
    foregroundColor: '#000', // qr-code color
    }} backgroundColor="#000" actionBackgroundColor="#333" size={1} labels={{
        action: "Написать сообщение",
        title: false,
        login: false,
}}/>
```
<img width="100" height="100" src="https://gitlab.mvk.com/vkapi/vk-qr-widget-component/raw/master/images/5.svg">

---
```
<QRWidget url="https://vk.com"
  qrOptions={{
    isShowLogo: true, // show logo in center
    logoColor: '#467FC2',
    logoData: false, // logo data in base64
    isShowBackground: true, // show qr-background
    backgroundColor: '#fff', // qr-code background color
    foregroundColor: '#000', // qr-code color
  }}
  backgroundColor="#467FC2" size={2}
  avatar="https://pp.userapi.com/c629123/v629123755/2ba65/029PylzKQLM.jpg"
  labels={{
    action: false,
    title: "happysanta.org",
    login: false,
  }}
/>
```
<img width="100" height="100" src="https://gitlab.mvk.com/vkapi/vk-qr-widget-component/raw/master/images/6.svg">

---
```
<QRWidget url="https://vk.com"
  qrOptions={{
    isShowLogo: true, // show logo in center
    logoColor: '#467FC2',
    logoData: false, // logo data in base64
    isShowBackground: true, // show qr-background
    backgroundColor: '#fff', // qr-code background color
    foregroundColor: '#000', // qr-code color
  }}
  backgroundColor="#467FC2" size={3}
  avatar="https://pp.userapi.com/c629123/v629123755/2ba65/029PylzKQLM.jpg"
  labels={{
    action: "Написать сообщение",
    title: "Happy Santa",
    login: "@hs",
  }}
/>
```
<img width="100" height="100" src="https://gitlab.mvk.com/vkapi/vk-qr-widget-component/raw/master/images/7.svg">

---
```
<QRWidget url="https://vk.com"
  qrOptions={{
    isShowLogo: true, // show logo in center
    logoColor: '#467FC2',
    logoData: false, // logo data in base64
    backgroundColor: '#fff', // qr-code background color
    foregroundColor: '#000', // qr-code color
  }}
  backgroundColor="#fff" titleForegroundColor="#000" loginForegroundColor="#457EC1" size={3}
  avatar="https://pp.userapi.com/c629123/v629123755/2ba65/029PylzKQLM.jpg"
  labels={{
    action: "Написать сообщение",
    title: "Happy Santa",
    login: "@hs",
  }}
/>
```
<img width="100" height="100" src="https://gitlab.mvk.com/vkapi/vk-qr-widget-component/raw/master/images/8.svg">

---
```
<QRWidget url="https://vk.com"
  qrOptions={{
    isShowLogo: true, // show logo in center
    logoColor: '#467FC2',
    logoData: false, // logo data in base64
    isShowBackground: true, // show qr-background
    backgroundColor: '#fff', // qr-code background color
    foregroundColor: '#000', // qr-code color
  }}
  withImageBackground={true}
  backgroundImage={bg} size={1}
  labels={{
    action: 'Написать сообщение',
    title: false,
    login: false,
  }}
/>
```
<img width="100" height="100" src="https://gitlab.mvk.com/vkapi/vk-qr-widget-component/raw/master/images/9.svg">

---
```
<QRWidget url="https://vk.com"
qrOptions={{
    isShowLogo: true, // show logo in center
    logoColor: '#467FC2',
    logoData: false, // logo data in base64
    isShowBackground: true, // show qr-background
    backgroundColor: '#fff', // qr-code background color
    foregroundColor: '#000', // qr-code color
    }}
    withImageBackground={true}
    backgroundImage={loveTree} size={3}
    labels={{
        action: false,
        title: 'Happy santa',
        login: '@hs',
}}
/>
```
<img width="100" height="100" src="https://gitlab.mvk.com/vkapi/vk-qr-widget-component/raw/master/images/10.svg">



