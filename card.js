export const generateCard = (qrSvg, props, state) => {
  const {isBigQr, withImageBackground, avatar, size} = props;
  const {averageBackgroundColor, login, title, action} = state;

  let actionBackgroundColor = this.getActionBackgroundColor();
  if (!login.path || !title.path || !action.path) {
    return '';
  }
  let body = '', defs = '';

  let {width, height} = this.getSize();

  let qrScale = 1;
  let qrPosition = {x: (width - 190) / 2, y: height - 190};
  if (isBigQr) {
    qrPosition = {x: 0, y: 220};
  }

  let actionPosition = {};
  if (action.value) {
    qrPosition.y -= 85;
  } else {
    qrPosition.y -= 30;
  }

  let actionDef = ``;
  let titleDef = ``;
  let loginDef = ``;
  let bgDef = ``;

  const avatarPosition = {x: (width - 56) / 2, y: 32};
  const titlePosition = {x: (width - title.metrics[0].width) / 2, y: avatarPosition.y + 56 + 33};
  const loginPosition = {x: (width - login.metrics[0].width) / 2, y: titlePosition.y + 25};
  const actionBackgroundPosition = {
    x: 0,
    y: height - ACTION_HEIGHT - ((action.path.length - 1) * ACTION_LINE_HEIGHT)
  };

  if (withImageBackground && this.getBackgroundImage()) {
    let image = new Image();
    image.src = this.getBackgroundImage();
    if (size === SIZE_LARGE_GRADIENT) {
      image.onload = () => {
        if (!averageBackgroundColor) {
          this.setState({averageBackgroundColor: Widget.getAverageColor(image)});
        }
      };

      if (!averageBackgroundColor) {
        return '';
      }
    }

    let backgroundImageHeight = size === SIZE_LARGE_GRADIENT ? 275 : height;
    body += `<image preserveAspectRatio="xMidYMid slice" x="0" y="0" width="${width}" height="${backgroundImageHeight}" xlink:href="${this.getBackgroundImage()}" />`;
    if (size === SIZE_LARGE_GRADIENT) {
      body += `
            <rect fill="url(#gradient-${this.getSuffix()})" x="0" y="184" width="${width}" height="90"/>
            <rect fill="${averageBackgroundColor}" x="0" y="274" width="${width}" height="${height}"/>`;
      bgDef = `
        <linearGradient id="gradient-${this.getSuffix()}" x1="0%" y1="0%" x2="0%" y2="100%">
          <stop offset="0%" style="stop-color:${averageBackgroundColor};stop-opacity:0" />
          <stop offset="100%" style="stop-color:${averageBackgroundColor};stop-opacity:1" />
        </linearGradient>`;
    }

    if (size === SIZE_LARGE_GRADIENT) {
      if (action.value) {
        actionBackgroundColor = this.getActionBackgroundColor(true);
      } else {
        qrPosition.y -= ACTION_HEIGHT;
      }

      titlePosition.y = height - ACTION_HEIGHT;
      loginPosition.y = titlePosition.y + 20;
    }
  } else {
    if (averageBackgroundColor) {
      this.setState({averageBackgroundColor: false});
    }
  }

  if (avatar && size !== SIZE_LARGE_GRADIENT) {
    body += `
        <circle cx="125" cy="60" r="30" fill="#fff"/>
        <image preserveAspectRatio="xMidYMid slice" clip-path="url(#avatar-mask-${this.getSuffix()})" x="${avatarPosition.x}" y="${avatarPosition.y}" width="56" height="56" xlink:href="${avatar}" />
        `;
  }

  if (title.value) {
    titleDef = `<g id="title-${this.getSuffix()}">${title.path[0]}</g>`;
    body += `<use x="${titlePosition.x}" y="${titlePosition.y}" xlink:href="#title-${this.getSuffix()}"/>`;
  }

  if (login.value) {
    loginDef = `<g id="login-${this.getSuffix()}">${login.path[0]}</g>`;
    body += `<use x="${loginPosition.x}" y="${loginPosition.y}" xlink:href="#login-${this.getSuffix()}"/>`;
  }

  if (action.value) {
    const actionBackgroundHeight = ACTION_HEIGHT + ((action.path.length - 1) * ACTION_LINE_HEIGHT);
    body += `<rect width="${width}" height="${actionBackgroundHeight}" transform="translate(${actionBackgroundPosition.x},${actionBackgroundPosition.y})" style="fill:${actionBackgroundColor};"/>`;
    actionPosition.y = actionBackgroundPosition.y + 30;
    qrPosition.y -= (action.path.length - 1) * ACTION_LINE_HEIGHT;
    for (let i = 0; i < action.path.length; i++) {
      body += `<use x="${(width - action.metrics[i].width) / 2}" y="${actionPosition.y}" xlink:href="#action-${this.getSuffix()}-${i}"/>`;
      actionPosition.y += ACTION_LINE_HEIGHT;
      actionDef += `<g id="action-${this.getSuffix()}-${i}">${action.path[i]}</g>`;
    }
  }

  body += `<use x="${qrPosition.x}" y="${qrPosition.y}" xlink:href="#qr-${this.getSuffix()}" transform="scale(${qrScale})" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"/>`

  return `
      <svg width="${width}" height="${height}" viewBox="0 0 ${width} ${height}" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <defs>
          <clipPath id="main-mask-${this.getSuffix()}">
            <rect x="0" y="0" width="${width}" height="${height}" rx="10"/>
          </clipPath>
          <clipPath id="avatar-mask-${this.getSuffix()}">
            <circle cx="125" cy="60" r="28" fill="#000"/>
          </clipPath>
          ${defs}
          ${bgDef}
          <g id="qr-${this.getSuffix()}">${qrSvg}</g>
          ${titleDef}
          ${loginDef}
          ${actionDef}
        </defs>
        <g clip-path="url(#main-mask-${this.getSuffix()})">
            <rect width="${width}" height="${height}" style="fill:${this.getBackgroundColor()}"/>
            ${body}
        </g>
      </svg>`;
}
