const vkQr = require('@vkontakte/vk-qr');
const fs = require('fs');
const path = require('path');
const { convert } = require('convert-svg-to-png');
const getCardSvg = require('./test-widget');
const VK = require('vk-io').VK;

const vk = new VK({
  token: '645feacb645feacb645feacb876433a5786645f645feacb390aab98a7d85cdc00f19029'
});

Object.defineProperty(Array.prototype, 'chunk_inefficient', {
  value: function(chunkSize) {
    var array = this;
    return [].concat.apply([],
      array.map(function(elem, i) {
        return i % chunkSize ? [] : [array.slice(i, i + chunkSize)];
      })
    );
  }
});


(async () => {
  const users = fs.readFileSync(path.resolve('./users.txt')).toString().split('\n');
  console.log(users.length);

  const userChunks = users.chunk_inefficient(50);

  for (let i = 0; i < userChunks.length; i++) {
    const chunk = userChunks[i];
    const idStr = chunk.join(',').toLowerCase().replace(/https:\/\/vk.com\//g, '');
    console.log(idStr);

    try {
      const response = await vk.api.users.get({
        user_ids: idStr,
        fields: 'screen_name,photo_200,has_photo'
      });

      console.log(response.length);

      for(let j = 0; j< response.length; j++) {
        const user = response[j];
        // console.log(user);
        const svg = getCardSvg('https://vk.com/' + user.screen_name, user.photo_200, [user.first_name, user.last_name].join(' '));
        const png = await convert(svg);

        fs.writeFileSync(path.resolve('./qr-export/' + user.screen_name + '.png'), png);
        console.log(i, j, ' completed')
      }
    } catch (e) {
      console.error(e);
    }
  }

  return;

  for (let i = 0; i < users.length; i++) {
    const user = users[i];
    const qrSvg = vkQr.createQR(user, {
      qrSize: 256,
      isShowLogo: true
    });

    const png = await convert(qrSvg);

    fs.writeFileSync(path.resolve('./qr-export/' + encodeURIComponent(user.replace('https://vk.com/', '')) + '.png'), png);
    console.log(i, ' completed')
  }

})();

